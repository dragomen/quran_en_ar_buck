import json
import pyaramorph.buckwalter as buck

eng_file = open("english_sahih.txt", "r")

#contains the English translation
quran_dict = {}

for line in eng_file:
        # print(type(lines))
        # ^ <class 'str'>
        _, chapter, verse, text = line.strip().split('||')
        quran_dict[(int(chapter), int(verse))] = text


with open('quran_buckwalter.json', 'r') as data_file:
    bk_dict = json.load(data_file)
    # print(type(bk_dict))
    #^ dict

#the final data structure used to export to a JSON file
full_dict = {}

for item in bk_dict:
    for key, value in item.items():

        #print(type(key))
        #str
        parts = key.split(":")
        #print(type(parts[0]))
        #str
        #print(quran_dict[int(parts[0]), int(parts[1])])
        #prints correctly

        inner_dict = {}

        inner_dict['arabic'] = buck.buck2uni(value)

        #print(buck.buck2uni(value))

        inner_dict['buckwalter'] = value

        #converting the strings in parts[] that fetches the
        #verse via [chapter, verse] in quran_dict
        inner_dict['translation'] = quran_dict[int(parts[0]), int(parts[1])]

        full_dict[key] = inner_dict


with open("quran_arabic_buckwalter_translation.json", "w+") as f:

    json.dump(full_dict, f)

    print("Done!")

eng_file.close()

'''
structure of new JSON:

[
    {
    'ch:v':
        {
          'buckwalter': 'text',
          'arabic': 'text',
          'translation': 'text'
            }
    },
    {
    'ch:v2':
        {
          'buckwalter': 'text',
          'arabic': 'text',
          'translation': 'text'
            }
    },
]
'''
